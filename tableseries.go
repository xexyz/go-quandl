package quandl

import (
	"fmt"
	"time"
	//"net/url"
)

// CharactersService handles communcation with the character related methods
// of the Marvel API
type CharactersService struct {
	client *Client
}

// Character represents a character

type CharacterDataWrapper struct {
	Code                                          int                         `json:"code"`
	Status                                        string                      `json:"status"`
	Copyright                                     string                      `json:"copyright"`
	AttributionText                               string                      `json:"attributionText"`
	AttributionHTML                               string                      `json:"attributionHTML"`
	Data                                          *CharacterDataContainer     `json:"data"`
	Etag                                          string                      `json:"etag"`
}

type CharacterDataContainer struct {
	Offset                                        int                         `json:"offset"`
	Limit                                         int                         `json:"limit"`
	Total                                         int                         `json:"total"`
	Count                                         int                         `json:"count"`
	Results                                       []*Character                `json:"results"`
}

type Character struct {
	ID                                            int                         `json:"id"`
	Name                                          string                      `json:"name"`
	Description                                   string                      `json:"description"`
	Modified                                      string                      `json:"modified"`  //TODO make this real time
	ResourceURI                                   string                      `json:"resourceURI"`
	Urls                                          []*Url                      `json:"urls"`
	Thumbnail                                     *Image                      `json:"thumbnail"`
	Comics                                        *ComicList                  `json:"comics"`
	Stories                                       *StoryList                  `json:"stories"`
	Events                                        *EventList                  `json:"events"`
	Series                                        *SeriesList                 `json:"series"`
}

type Url struct {
	Type                                          string                      `json:"type"`
	Url                                           string                      `json:"url"`
}

type Image struct {
	Path                                          string                      `json:"path"`
	Extension                                     string                      `json:"extension"`
}

type ComicList struct {
	Available                                     int                         `json:"available"`
	Returned                                      int                         `json:"returned"`
	CollectionURI                                 string                      `json:"collectionURI"`
	Items                                         []*ComicSummary             `json:"items"`
}

type ComicSummary struct {
	ResourceURI                                   string                      `json:"resourceURI"`
	Name                                          string                      `json:"name"`
}

type StoryList struct {
	Available                                     int                         `json:"available"`
	Returned                                      int                         `json:"returned"`
	CollectionURI                                 string                      `json:"collectionURI"`
	Items                                         []*StorySummary             `json:"items"`
}

type StorySummary struct {
	ResourceURI                                   string                      `json:"resourceURI"`
	Name                                          string                      `json:"name"`
	Type                                          string                      `json:"type"`
}

type EventList struct {
	Available                                     int                         `json:"available"`
	Returned                                      int                         `json:"returned"`
	CollectionURI                                 string                      `json:"collectionURI"`
	Items                                         []*EventSummary             `json:"items"`
}

type EventSummary struct {
	ResourceURI                                   string                      `json:"resourceURI"`
	Name                                          string                      `json:"name"`
}

type SeriesList struct {
	Available                                     int                         `json:"available"`
	Returned                                      int                         `json:"returned"`
	CollectionURI                                 string                      `json:"collectionURI"`
	Items                                         []*SeriesSummary            `json:"items"`
}

type SeriesSummary struct {
	ResourceURI                                   string                      `json:"resourceURI"`
	Name                                          string                      `json:"name"`
}

func (s CharacterDataWrapper) String() string {
	return Stringify(s)
}

// ListCharactersOptions represents the available Characters() options.
type GetCharactersOptions struct {
	Name           *string
	NameStartsWith *string
	ModifiedSince  *time.Time
	Comics         *int
	Series         *int
	Events         *int
	Stories        *int
	OrderBy        *string
	Limit          *int
	Offset         *int
}

// GetCharacters gets a list of characters
func (s *CharactersService) GetCharacter(id int, opt *GetCharactersOptions, options ...OptionFunc) (*CharacterDataWrapper, *Response, error) {
	u := fmt.Sprintf("characters/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CharacterDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		 return nil, resp, err
	}

	return c, resp, err
}
