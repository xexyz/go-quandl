package quandl

import (
	"fmt"
	// "time"
	//"net/url"
)

type TimeSeriesService service

type DataSetData struct {
	Limit               int           `json:"limit"`
	Transform           string        `json:"transform"`
	ColumnIndex         int           `json:"column_index"`
	ColumnNames         []string      `json:"column_names"`
	StartDate           string        `json:"start_date"`
	EndDate             string        `json:"end_date"`
	Frequency           string        `json:"frequency"`
	Data                [][]string    `json:"data"`
	Collapse            string        `json:"collapse"`
	Order               string        `json:"order"`
}

type DataSetRoot struct{
	DataSet             *DataSet      `json:"dataset"` 
}

type DataSet struct {
	ID                  int              `json:"id"`
	DataSetCode         string           `json:"dataset_code"`
	DataBaseCode        string           `json:"database_code"`
	Name                string           `json:"name"`
	Description         string           `json:"description"`
	RefreshedAt         string           `json:"refreshed_at"`
	NewestAvailableDate string           `json:"newest_available_date"`
	OldestAvailableDate string           `json:"oldest_available_date"`
	ColumnNames         []string         `json:"column_names"`
	Frequency           string           `json:"frequency"`
	Type                string           `json:"type"`
	Premium             bool             `json:"premiumn"`
	Limit               int              `json:"limit"`
	Transform           string           `json:"transform"`
	ColumnIndex         int              `json:"column_index"`
	StartDate           string           `json:"start_date"`
	EndDate             string           `json:"end_date"`
	Data                [][]interface{}  `json:"data"`
	Collapse            string           `json:"collapse"`
	Order               string           `json:"order"`
	DatabaseId          int              `json:"database_id"`
}

func (s DataSetData) String() string {
	return Stringify(s)
}

func (s DataSet) String() string {
	return Stringify(s)
}

type GetTimeSeriesOptions struct {
	Limit             int        `url:"limit,omitempty"`
	StartDate         string     `url:"start_date,omitempty"`
	EndDate           string     `url:"end_date,omitempty"`
	
}

type GetTimeSeriesDataOptions struct {
}

func (s *TimeSeriesService) GetTimeSeries(databaseCode string, datasetCode string, opt *GetTimeSeriesOptions) (*DataSet, *Response, error) {
	u := fmt.Sprintf("datasets/%s/%s", databaseCode, datasetCode)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *DataSetRoot
	resp, err := s.client.Do(req, &c)
	
	if err != nil {
		return nil, resp, err
	}

	return c.DataSet, resp, err
}

func (s *TimeSeriesService) GetTimeSeriesData(databaseCode string, datasetCode string, opt *GetTimeSeriesDataOptions) (*DataSetData, *Response, error) {
	u := fmt.Sprintf("datasets/%s/%s/data", databaseCode, datasetCode)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *DataSetData
	resp, err := s.client.Do(req, &c)
	if err != nil {
		 return nil, resp, err
	}

	return c, resp, err
}
