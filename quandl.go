package quandl

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	// "reflect"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"

	"github.com/google/go-querystring/query"
)

const (

	libraryVersion = "1"
	defaultBaseURL = "https://www.quandl.com/api/v3/"
	userAgent      = "go-quandl/" + libraryVersion
	apiDocs        = "https://docs.quandl.com/docs"
)
type Client struct {
	client *http.Client
	baseURL *url.URL

	appKey string
	dataFormat string
	UserAgent string
	TimeSeries             *TimeSeriesService
	// TableSeries            *TableSeriesService
}

type service struct {
        client *Client
}

// // addOptions adds the parameters in opt as URL query parameters to s. opt
// // must be a struct whose fields may contain "url" tags.
// func addOptions(s string, opt interface{}) (string, error) {
// 	v := reflect.ValueOf(opt)
// 	if v.Kind() == reflect.Ptr && v.IsNil() {
// 		return s, nil
// 	}

// 	u, err := url.Parse(s)
// 	if err != nil {
// 		return s, err
// 	}

// 	qs, err := query.Values(opt)
// 	if err != nil {
// 		return s, err
// 	}

// 	u.RawQuery = qs.Encode()
// 	return u.String(), nil
// }

func NewClient(httpClient *http.Client, appKey string, dataFormat string) *Client{
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	c:= &Client{client: httpClient,
		appKey: appKey,
		UserAgent: userAgent,
		dataFormat: dataFormat,
	}
	if err := c.SetBaseURL(defaultBaseURL); err != nil {
		panic(err)
	}

	c.TimeSeries = &TimeSeriesService{client: c}
	// c.TableSeries = &TableSeriesService{client: c}

	return c
}

func (c *Client) BaseURL() *url.URL {
	u := *c.baseURL
	return &u
}

func (c *Client) SetBaseURL(urlStr string) error {
	// Make sure the given URL end with a slash
	if !strings.HasSuffix(urlStr, "/") {
		urlStr += "/"
	}

	var err error
	c.baseURL, err = url.Parse(urlStr)
	return err
}

//
func (c *Client) NewRequest(method, path string, opt interface{}) (*http.Request, error) {
	u := *c.baseURL
	// Set the encoded opaque data

	p := fmt.Sprintf("%s.%s", path, c.dataFormat)
	u.Opaque = c.baseURL.Path + p

	a := u.Query()
	a.Set("api_key", c.appKey)
	u.RawQuery = a.Encode()

	if opt != nil {
		q, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		u.RawQuery += "&" + q.Encode()
	}
   
	req := &http.Request{
		Method:         method,
		URL:            &u,
		Proto:          "HTTP/1.1",
		ProtoMajor:     1,
		ProtoMinor:     1,
		Header:         make(http.Header),
		Host:           u.Host,
	}

	req.Header.Set("Accept", "application/json")

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	return req, nil
}

type Response struct {
	*http.Response
}

func newResponse(r *http.Response) *Response {
	response := &Response{Response: r}
	return response
}


func (c *Client) Do (req *http.Request, v interface{}) (*Response, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	
	response := newResponse(resp)
	err = CheckResponse(resp)
	if err != nil {
		//even though there was an error, we still return the response
		// in case the caller wants to inspect it further
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}

	return response, err
}


// An ErrorResponse reports one or more errors caused by an API request.
//
// GitLab API docs:
// https://gitlab.com/gitlab-org/gitlab-ce/blob/8-16-stable/doc/api/README.md#data-validation-and-error-reporting
type ErrorResponse struct {
	Response *http.Response
	Message  string
}

func (e *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(e.Response.Request.URL.Opaque)
	u := fmt.Sprintf("%s://%s%s", e.Response.Request.URL.Scheme, e.Response.Request.URL.Host, path)
	return fmt.Sprintf("%s %s: %d %s", e.Response.Request.Method, u, e.Response.StatusCode, e.Message)
}

// CheckResponse checks the API response for errors, and returns them if present.
func CheckResponse(r *http.Response) error {
	switch r.StatusCode {
	case 200, 201, 304:
		return nil
	}

	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		var raw interface{}
		if err := json.Unmarshal(data, &raw); err != nil {
			errorResponse.Message = "failed to parse unknown error format"
		}

		errorResponse.Message = parseError(raw)
	}

	return errorResponse
}

// Format:
// {
//     "message": {
//         "<property-name>": [
//             "<error-message>",
//             "<error-message>",
//             ...
//         ],
//         "<embed-entity>": {
//             "<property-name>": [
//                 "<error-message>",
//                 "<error-message>",
//                 ...
//             ],
//         }
//     },
//     "error": "<error-message>"
// }
func parseError(raw interface{}) string {
	switch raw := raw.(type) {
	case string:
		return raw

	case []interface{}:
		var errs []string
		for _, v := range raw {
			errs = append(errs, parseError(v))
		}
		return fmt.Sprintf("[%s]", strings.Join(errs, ", "))

	case map[string]interface{}:
		var errs []string
		for k, v := range raw {
			errs = append(errs, fmt.Sprintf("{%s: %s}", k, parseError(v)))
		}
		sort.Strings(errs)
		return strings.Join(errs, ", ")

	default:
		return fmt.Sprintf("failed to parse unexpected error type: %T", raw)
	}
}

// OptionFunc can be passed to all API requests to make the API call as if you were
// another user, provided your private token is from an administrator account.
//
// GitLab docs: https://gitlab.com/gitlab-org/gitlab-ce/blob/8-16-stable/doc/api/README.md#sudo
type OptionFunc func(*http.Request) error

// WithSudo takes either a username or user ID and sets the SUDO request header
func WithSudo(uid interface{}) OptionFunc {
	return func(req *http.Request) error {
		switch uid := uid.(type) {
		case int:
			req.Header.Set("SUDO", strconv.Itoa(uid))
			return nil
		case string:
			req.Header.Set("SUDO", uid)
			return nil
		default:
			return fmt.Errorf("uid must be either a username or user ID")
		}
	}
}

// WithContext runs the request with the provided context
func WithContext(ctx context.Context) OptionFunc {
	return func(req *http.Request) error {
		*req = *req.WithContext(ctx)
		return nil
	}
}

// to store v and returns a pointer to it.
func Bool(v bool) *bool {
	p := new(bool)
	*p = v
	return p
}

// Int is a helper routine that allocates a new int32 value
// to store v and returns a pointer to it, but unlike Int32
// its argument value is an int.
func Int(v int) *int {
	p := new(int)
	*p = v
	return p
}

// String is a helper routine that allocates a new string value
// to store v and returns a pointer to it.
func String(v string) *string {
	p := new(string)
	*p = v
	return p
}

// // AccessLevel is a helper routine that allocates a new AccessLevelValue
// // to store v and returns a pointer to it.
// func AccessLevel(v AccessLevelValue) *AccessLevelValue {
// 	p := new(AccessLevelValue)
// 	*p = v
// 	return p
// }

// // NotificationLevel is a helper routine that allocates a new NotificationLevelValue
// // to store v and returns a pointer to it.
// func NotificationLevel(v NotificationLevelValue) *NotificationLevelValue {
// 	p := new(NotificationLevelValue)
// 	*p = v
// 	return p
// }

// // VisibilityLevel is a helper routine that allocates a new VisibilityLevelValue
// // to store v and returns a pointer to it.
// func VisibilityLevel(v VisibilityLevelValue) *VisibilityLevelValue {
// 	p := new(VisibilityLevelValue)
// 	*p = v
// 	return p
// }
